/*** Keeping references on DOM elements for further use ***/

let buttonSpawn = document.getElementById("buttonSpawn")
let formSpawn = document.getElementById("formSpawn")
let imageSpawn = document.getElementById("imageSpawn")
let scoreArea = document.getElementById("scoreArea")
let timerArea = document.getElementById("timerArea")
let startCats = document.getElementById("startCats")
let startCars = document.getElementById("startCars")
let startBeers = document.getElementById("startBeers")
let startTissues = document.getElementById("startTissues")

/*** Object representing best player times for each Memory game theme ***/

let bestTimes = {
    cars: {name: "", time: ""},
    cats: {name: "", time: ""},
    beers: {name: "", time: ""},
    tissues: {name: "", time: ""}
}

/*** Function used to update and rerender best player times in main menu ***/

function getBestTimes(){
    if(localStorage.getItem("Cats")){
        bestTimes.cats = JSON.parse(localStorage.getItem("Cats"))
    } else bestTimes.cats = "00:00"

    document.getElementById("scoreCats").innerText = "Name: " +
        (bestTimes.cats.name ? bestTimes.cats.name : "None") + " ended in: " +
        (bestTimes.cats.time ? bestTimes.cats.time : "None")  + " seconds."

    if(localStorage.getItem("Cars")){
        bestTimes.cars = JSON.parse(localStorage.getItem("Cars"))
    } else bestTimes.cars = "00:00"

    document.getElementById("scoreCars").innerText = "Name: " +
        (bestTimes.cars.name ? bestTimes.cars.name : "None") + " ended in: " +
        (bestTimes.cars.time ? bestTimes.cars.time : "None")  + " seconds."

    if(localStorage.getItem("Beers")){
        bestTimes.beers = JSON.parse(localStorage.getItem("Beers"))
    } else bestTimes.beers = "00:00"

    document.getElementById("scoreBeers").innerText = "Name: " +
        (bestTimes.beers.name ? bestTimes.beers.name : "None") + " ended in: " +
        (bestTimes.beers.time ? bestTimes.beers.time : "None")  + " seconds."

    if(localStorage.getItem("Tissues")){
        bestTimes.tissues = JSON.parse(localStorage.getItem("Tissues"))
    } else bestTimes.tissues = "00:00"

    document.getElementById("scoreTissues").innerText = "Name: " +
        (bestTimes.tissues.name ? bestTimes.tissues.name : "None") + " ended in: " +
        (bestTimes.tissues.time ? bestTimes.tissues.time : "None") + " seconds."

}

/*** Helper function used to clear screen and show menu buttons after game finishes ***/

function resetElements(){
    imageSpawn.innerHTML = "";
    formSpawn.innerHTML = "";
    formSpawn.style.visibility = "hidden";
    buttonSpawn.style.display = "flex";
    timerArea.innerText = "01:30";
    scoreArea.innerText = "Score: 0";
    timerArea.style.visibility = "hidden";
    scoreArea.style.visibility = "hidden";
}


/*** Class representing whole game, uses variables such as: timer, contains instances GameObject etc. ***/

class Game{

    constructor(timeLimit,theme) {
        this.timer = timeLimit;
        this.StartingTime = timeLimit;
        this.gameFinished = false;
        this.theme = theme;
        this.gameObjects = [];
        this.winner = false;
        this.score = 0;

        this.firstTurned = null;
        this.secondTurned = null;
        this.canTurn = true;
    }

    /*** Function used to duplicate each image in resources and then randomize them. After that, each image is
     * set as front background of Memory game card. After click on image, handler function is called.
     */

    setupImages(){
        for(let i = 1; i <= 9; i++){
            for(let j = 1; j <= 2; j++){
                this.gameObjects.push(new GameObject(i,j));
            }
        }

        this.randomizeImages();

        imageSpawn.innerHTML = "";

        this.gameObjects.forEach((image) => {
            let outer = document.createElement("div");
            outer.classList.add("cardOuter")

            let inner = document.createElement("div");
            inner.classList.add("cardInner")
            inner.classList.add(image.getId());

            let front = document.createElement("div");
            let back = document.createElement("div");
            back.classList.add("back");
            front.classList.add("front");

            inner.appendChild(front);
            inner.appendChild(back);


            front.style.backgroundImage = "url(Resources/" + this.theme + "/" + image.getId() + ".jpg)";
            front.style.backgroundPosition = "center"
            front.style.backgroundSize = "cover"

            back.style.backgroundImage = "url(Resources/" + this.theme + "/back.png)";
            back.style.backgroundPosition = "center"
            back.style.backgroundSize = "cover"


            outer.appendChild(inner)
            outer.addEventListener("click",() => this.handleClick(image,inner))

            imageSpawn.appendChild(outer);
        })
    }

    randomizeImages(){
        for (let i = this.gameObjects.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = this.gameObjects[i];
            this.gameObjects[i] = this.gameObjects[j];
            this.gameObjects[j] = temp;
        }
    }


    /*** Function, which is used to set whole game - sets interval, in which is timer updated.
     * Also hides main menu buttons.
     ***/

    init(){
        this.setupImages();

        timerArea.style.visibility = "visible";
        scoreArea.style.visibility = "visible";
        buttonSpawn.style.display = "none";

        this.Countdown = setInterval(() => this.gameOrder(),1000);
    }

    /*** Image click handler, which decides, if card should be turned, checks if both cards are same (but different sides)
     * and leaves Found cards turned. Success sound is played, after player finds two sides of same picture.
     * When player turns different images, he is penalized with 3 seconds timeout.
     */

    handleClick(gameObject,div){

        if(this.canTurn && gameObject.getState() === "NOT FOUND" && gameObject !== this.firstTurned && !this.gameFinished){

            div.classList.toggle("cardFlipped")

            if(!this.firstTurned){

                this.firstTurned = gameObject;

            } else if(!this.secondTurned) {

                this.secondTurned = gameObject;
                this.canTurn = false;

                if (this.firstTurned.getId() === this.secondTurned.getId() && this.firstTurned.getSide() !== this.secondTurned.getSide()) {

                    let audio = new Audio("Resources/Sound/successRing.m4a");
                    this.score++;

                    this.firstTurned.setFound();
                    this.secondTurned.setFound();

                    scoreArea.innerHTML = "Score: " + this.score;

                    setTimeout(() => {
                        audio.play();
                        this.firstTurned = null
                        this.secondTurned = null;
                        this.canTurn = true;
                    }, 100)

                } else {

                    setTimeout(() => {
                        let imgs = document.querySelectorAll(".cardFlipped");
                        for (let i = 0; i < imgs.length; i++) {
                            if(imgs[i].classList.contains(this.firstTurned.getId()) || (imgs[i].classList.contains(this.secondTurned.getId()))){
                                imgs[i].classList.toggle("cardFlipped")
                            }
                        }

                        this.firstTurned = null
                        this.secondTurned = null;
                        this.canTurn = true;

                    }, 1000)
                }
            }
        }
    }


    /*** Function used to update time. When time runs out, game is set as Finished. ***/

    updateTime(){

        if(this.timer > 0) {

            this.timer--;

            if(this.timer < 6){
                timerArea.style.color = "red";
            } else {
                timerArea.style.color = "black";
            }

            let Minutes = (Math.floor(this.timer / 60)).toString();
            let Seconds = (this.timer - (Minutes * 60)).toString();

            if (Minutes.length < 2) {
                Minutes = "0" + Minutes;
            }
            if (Seconds.length < 2) {
                Seconds = "0" + Seconds;
            }

            timerArea.innerHTML = Minutes + ":" + Seconds;

        } else {
            this.winner = false;
            this.gameFinished = true;
        }

    }

    /*** Function, which controls each cycle of game. Also checks, if all pictures are found. ***/

    gameOrder(){

        if(!this.gameFinished){

            if(this.score === this.gameObjects.length/2){
                this.gameFinished = true;
                this.winner = true;
            } else {
                this.updateTime();
            }
        } else {
            clearInterval(this.Countdown)
            this.endGame();
        }
    }


    /*** Function used to return to main menu instantly, if player loses. If player won, input field is set
     * and player can enter his name. If game time was better than best time, this player becomes the best
     * and best save in localstorage is overwritten.
     */

    endGame(){
        if(this.winner){

            formSpawn.innerHTML = "";
            formSpawn.style.visibility = "visible"

            let input = document.createElement("input");
            input.placeholder = "Enter player name...";
            input.type = "text";

            let sendButton = document.createElement("button")
            sendButton.innerText = "Save result";

            formSpawn.appendChild(input);
            formSpawn.appendChild(sendButton);

            sendButton.addEventListener("click", (e) => this.saveScore(input.value,e));
        } else {
            resetElements();
            getBestTimes();
            alert("You've lost!")

        }
    }

    /*** Function, which is used to save score, if player is better than the best time. Otherwise nothing is saved. ***/

    saveScore(name,e){

        let input = document.querySelector("input")

        if(name.trim() === ""){
            e.preventDefault()
            input.style.borderColor = "red"

        } else {

            input.style.borderColor = "black"

            if(localStorage.getItem(this.theme)){
                let bestTime = JSON.parse(localStorage.getItem(this.theme));
                bestTime = parseInt(bestTime.time);

                let newTime = {
                    name: name,
                    time: this.StartingTime - this.timer
                }

                if(bestTime > newTime.time){
                    localStorage.setItem(this.theme,JSON.stringify(newTime))
                }

            } else {

                let newTime = {
                    name: name,
                    time: this.StartingTime - this.timer
                }

                localStorage.setItem(this.theme,JSON.stringify(newTime))
            }

            resetElements();
            getBestTimes();

        }
    }

}

/*** Class representing each card of Memory game ***/

class GameObject{
    constructor(id, side) {
        this.id = id;
        this.side = side;
        this.state = "NOT FOUND"
    }

    getId(){
        return this.id;
    }

    getSide(){
        return this.side;
    }

    getState(){
        return this.state;
    }

    setFound(){
        this.state = "FOUND";
    }
}


getBestTimes();

startCats.addEventListener("click",() =>{
    let game = new Game(90,"Cats");
    game.init();
})

startCars.addEventListener("click",() =>{
    let game = new Game(90,"Cars");
    game.init();
})

startBeers.addEventListener("click",() =>{
    let game = new Game(90,"Beers");
    game.init();
})

startTissues.addEventListener("click",() =>{
    let game = new Game(90,"Tissues");
    game.init();
})
